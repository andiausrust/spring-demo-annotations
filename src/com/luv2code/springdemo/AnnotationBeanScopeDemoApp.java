package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeDemoApp {

	public static void main(String[] args) {
		
		//load spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//get bean from spring
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		Coach alphaCoach = context.getBean("tennisCoach", Coach.class);

		//check if they are the same, print result
		boolean compare = (theCoach==alphaCoach);
		System.out.println(compare);
		System.out.println(theCoach);
		System.out.println(alphaCoach);

		// context close
		context.close();
	}

}
