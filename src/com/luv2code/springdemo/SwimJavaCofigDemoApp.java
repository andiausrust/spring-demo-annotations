package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SwimJavaCofigDemoApp {

	public static void main(String[] args) {
		
		//read spring config file
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//get a bean from spring container
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		
		//use a method on a bean
		System.out.println(theCoach.getDailyFortune());
		
		//close context
		context.close();
	}

}
