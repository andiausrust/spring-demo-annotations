package com.luv2code.springdemo;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component 
//@Scope("prototype")
public class TennisCoach implements Coach {
	
	@Autowired
	@Qualifier("randomFortuneService")
	private FortuneService fortuneService;
	
	/*@Autowired  
	public TennisCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}*/
	
	// define init method
	@PostConstruct
	public void doMyStartupStuff() {
		System.out.println("<<inside doMyStartUpStuff");
	}
	
	//define my destroy method
	@PreDestroy
	public void doMyCleanUpStuff() {
		System.out.println("<<inside doMyCleanUpStuff");
	}

	public TennisCoach() {
		System.out.println("this is the Tennis Coach constructor");
	}
	
	@Override
	public String getDailyWorkout() {
		return "exercise your footwork";
	}

	/*define a setter method
	@Autowired
	public void doSomeCrazy(FortuneService theFortuneService) {
		System.out.println("Tennis coach inside the crazy stuff");
		fortuneService=theFortuneService;
	}*/
	
	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

	@Override
	public String getEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTeam() {
		// TODO Auto-generated method stub
		return null;
	}
}
